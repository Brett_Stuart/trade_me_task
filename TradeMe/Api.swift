//
//  Api.swift
//  TradeMe
//
//  Created by Brett on 2016/12/04.
//  Copyright © 2016 Brett. All rights reserved.
//

import Alamofire
import SwiftyJSON


protocol ApiDelegate: class {
    func didGetCategories(arrCategories: [[String:AnyObject]])
    func didGetListings(arrListings: [[String:AnyObject]])
}

class Api{
    
    
    static var apiInstance: Api?
    weak var delegate:ApiDelegate?
    
    static var getApiInstance: Api{
        get{
            if(self.apiInstance == nil){
                self.apiInstance = Api()
            }
            return self.apiInstance!
        }
    }
    
    
    func getCategories(categoryNumber: String){
        
        let urlString = Constants.CATEGORY_URL + categoryNumber + ".json"
        
        Alamofire.request(urlString, method: .get, parameters: ["depth":"2","with_counts ":"true"], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    let swiftyJsonVar = JSON(response.result.value!)
                    var arrRes = [[String:AnyObject]]()
                    
                    if let resData = swiftyJsonVar["Subcategories"].arrayObject {
                        arrRes = resData as! [[String:AnyObject]]
                    }
                    if arrRes.count > 0 {
                        Api.getApiInstance.delegate?.didGetCategories(arrCategories: arrRes)
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                break
                
            }
        }
    }
    
    func getListings(categoryNumber: String){
        
        let urlString = Constants.SEARCH_URL+".json"
        let auth = "OAuth oauth_consumer_key=\"" + Constants.CONSUMER_KEY + "\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"" + Constants.CONSUMER_SECRET + "%26\""
        let headers = [
            "Authorization": auth
        ]

        Alamofire.request(urlString, method: .get, parameters: ["category":categoryNumber,"rows":"20"], encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if response.result.value != nil{
                    let swiftyJsonVar = JSON(response.result.value!)
                    var arrRes = [[String:AnyObject]]()
                    print(response.result.value!)
                    if let resData = swiftyJsonVar["List"].arrayObject {
                        arrRes = resData as! [[String:AnyObject]]
                    }
                    if arrRes.count > 0 {
                        Api.getApiInstance.delegate?.didGetListings(arrListings: arrRes)
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                break
                
            }
        }
    }
}
