//
//  CategoriesTableViewCell.swift
//  TradeMe
//
//  Created by Brett on 2016/12/04.
//  Copyright © 2016 Brett. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell{
    
    @IBOutlet var categoryDescription: UILabel?
    @IBOutlet var categoryListingCount: UILabel?
   
}
