//
//  ListingTableViewCell.swift
//  TradeMe
//
//  Created by Brett on 2016/12/05.
//  Copyright © 2016 Brett. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell{
    
    @IBOutlet var listingThumbnail: ImageLoaderImageView?
    @IBOutlet var listingTitle: UILabel?
    @IBOutlet var listingId: UILabel?
    
}
