//
//  ImageLoaderImageView.swift
//  TradeMe
//
//  Created by Brett on 2016/12/05.
//  Copyright © 2016 Brett. All rights reserved.
//

import Foundation
import UIKit

class ImageLoaderImageView : UIImageView {
    
    public func imageFromServerURL(urlString: String) {
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
            
        }).resume()
    }
}
