//
//  CategoriesViewController.swift
//  TradeMe
//
//  Created by Brett on 2016/12/04.
//  Copyright © 2016 Brett. All rights reserved.
//



import UIKit

class CategoriesViewController : UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate, DataModelDelegate{
    
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var showButton: UIButton?
    var arrListings: [[String:AnyObject]]?
    
    
    override func viewDidLoad() {
        
        tableView?.delegate = self
        DataModel.getModelInstance.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(CategoriesViewController.didSelectCategory), name: NSNotification.Name(rawValue: "didSelectCategory"), object: nil)
        
    }
    
    func didGetCategories(arrCategories: [[String:AnyObject]]){}
    
    func didGetListings(arrListings: [[String:AnyObject]]){}
    
    func didSelectCategory(){
        self.arrListings = DataModel.getModelInstance.arrListings
        tableView?.dataSource = self
        tableView?.reloadData()
    }
    
    @IBAction func showCategoriesPopover(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "showCategories", sender: self)
                        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "showCategories"){
            
            let nav = segue.destination as! UINavigationController
            let popvc = nav.topViewController as! CategoriesPopOverViewController
            popvc.setCategoryNameNumber(categoryName: "", categoryNumber: "0")
            
            let viewController  = popvc.popoverPresentationController
            
            if(viewController != nil)
            {
                viewController?.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return .none
        
    }
    
    //MARK: - Tableview Delegate & Datasource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
        
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return (self.arrListings?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListingTableViewCell", for: indexPath) as! ListingTableViewCell
        var dict = arrListings?[indexPath.row]
        cell.listingTitle?.text = dict?["Title"] as? String
        cell.listingId?.text = dict?["ListingId"] as? String
        let url = dict?["PictureHref"] as? String
        if(url != nil){
            cell.listingThumbnail?.imageFromServerURL(urlString: url!)
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
     
    }
    
}
