//
//  Data.swift
//  TradeMe
//
//  Created by Brett on 2016/12/04.
//  Copyright © 2016 Brett. All rights reserved.
//

protocol DataModelDelegate: class {
    func didGetCategories(arrCategories: [[String:AnyObject]])
    func didGetListings(arrListings: [[String:AnyObject]])
}

import Foundation


class DataModel : ApiDelegate{
    
    weak var delegate:DataModelDelegate?
    static var modelInstance: DataModel?
    var arrListings: [[String:AnyObject]]?

    static var getModelInstance: DataModel{
        get{
            
            if(self.modelInstance == nil){
                modelInstance = DataModel()
            }
            return modelInstance!
        }
    }

    init() {
        Api.getApiInstance.delegate = self
    }

    func getCategories(categoryNumber: String){
    
        Api.getApiInstance.getCategories(categoryNumber: categoryNumber)
        Api.getApiInstance.getListings(categoryNumber: categoryNumber)
    }
    
    func getListings(categoryNumber: String){
        
        Api.getApiInstance.getListings(categoryNumber: categoryNumber)
    }
    
    func didGetCategories(arrCategories: [[String:AnyObject]]){
        
        DataModel.getModelInstance.delegate?.didGetCategories(arrCategories: arrCategories)
    }
    
    func didGetListings(arrListings: [[String:AnyObject]]){
        print(arrListings)
        self.arrListings = arrListings
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didSelectCategory"), object: nil)
        
    }
    
}
