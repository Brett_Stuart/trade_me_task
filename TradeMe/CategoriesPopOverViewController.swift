//
//  CategoriesPopOverViewController.swift
//  TradeMe
//
//  Created by Brett on 2016/12/04.
//  Copyright © 2016 Brett. All rights reserved.
//

import UIKit

class CategoriesPopOverViewController : UIViewController, DataModelDelegate, UITableViewDataSource, UITableViewDelegate {
   
   
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var doneButton: UIButton?
    var dataModel: DataModel?
    var arrCategories: [[String:AnyObject]]?
    var categoryName: String?
    var categoryNumber: String?
    
    override func viewDidLoad() {
        
        tableView?.delegate = self
        DataModel.getModelInstance.delegate = self
        
    }
    
    @IBAction func dismissPopOver() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func didGetCategories(arrCategories: [[String:AnyObject]]){
        
        
        self.arrCategories = arrCategories
        tableView?.dataSource = self
        tableView?.reloadData()
        
    }
    func didGetListings(arrListings: [[String:AnyObject]]){
           
    }
    
    func setCategoryNameNumber(categoryName: String, categoryNumber: String){
        self.categoryName = categoryName
        self.categoryNumber = categoryNumber
        DataModel.getModelInstance.getCategories(categoryNumber: categoryNumber)
        
    }
    
    //MARK: - Tableview Delegate & Datasource
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return (self.arrCategories?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell", for: indexPath) as! CategoriesTableViewCell
        var dict = arrCategories?[indexPath.row]
        cell.categoryDescription?.text = dict?["Name"] as? String
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        var dict = arrCategories?[indexPath.row]
        if (dict?["Subcategories"]) == nil {
            self.dismiss(animated: true, completion: nil)
            return
        }
        let name = dict?["Name"] as? String
        let number = dict?["Number"] as? String
        
        let categoriesPopOverViewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesPopOverViewController") as! CategoriesPopOverViewController
        categoriesPopOverViewController.setCategoryNameNumber(categoryName: name!, categoryNumber: number!)
        self.navigationController?.pushViewController(categoriesPopOverViewController, animated: true)
        
    }

    
}
